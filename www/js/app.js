angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var diametrix = angular.module('diametrix', ['ionic','firebase','ngCordova','chart.js','diametrix.directives'])

.value("AUTHREF", new Firebase("https://diametrix.firebaseio.com/") )

.config(function($stateProvider, $urlRouterProvider){

$stateProvider


.state('login',{
  url:'/login',
  templateUrl: 'views/login.html',
  controller: 'LoginCtrl'
})

.state('register',{
  url:'/register',
  templateUrl: 'views/register.html'
})

.state('forgotPassword',{
  url:'/forgotPassword',
  templateUrl: 'views/forgotPassword.html',
})

.state('advanced',{
  url:'/advanced',
  templateUrl: 'views/advanced.html',
  //controller: 'AdvancedCtrl'
})

.state('profile',{
  url:'/profile',
  templateUrl: 'views/profile.html',
  controller:'profileCtrl'
  
})
.state('schedule',{
  url:'/schedule',
  templateUrl: 'views/schedule.html'
})


.state('reminders',{
  url:'/reminders',
  templateUrl: 'views/reminders.html',
  
})

.state('alert',{
  url:'/alert',
  templateUrl: 'views/alert.html'

})

.state('diet',{
  url:'/diet',
  templateUrl: 'views/diet.html',
  
})

.state('tracking',{
  url:'/tracking',
  templateUrl: 'views/tracking.html'
})

.state('Blog',{
  url:'/Blog',
  templateUrl: 'views/Blog.html',
  
})

.state('diametrixblog',{
  url:'/diametrixblog',
  templateUrl: 'views/diametrixblog.html',
  
})

.state('dashboard',{
  url:'/dashboard',
  templateUrl: 'views/dashboard.html',
  
})

.state('reports',{
  url:'/reports',
  templateUrl: 'views/rptshare.html',
  
})

$urlRouterProvider.otherwise('/login');


})

.controller('LoginCtrl', function($scope,$firebaseAuth, AUTHREF,$state, $ionicLoading,$ionicSideMenuDelegate)  {
$ionicSideMenuDelegate.canDragContent(false);

$scope.login=function(_email,_password)
{
  $ionicLoading.show({
      template: 'Loging in ...'
    });
$firebaseAuth(AUTHREF).$authWithPassword({
  email:_email,
  password:_password
}).then (function(authData){
   console.log("Logged in as:", authData.uid);
   $state.go('dashboard');
   $ionicLoading.hide();

}).catch(function(error){
  $scope.errors = err;
  $ionicLoading.hide();
  console.log("Error :", error);
})

}



$scope.profile = function(){
  $sate.go('profile');

}
$scope.schedule = function(){
  $sate.go('schedule');
}

})
angular.module('diametrix.directives', [])
.directive('validationCheck', ['$parse', function ($parse) {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ngModel) {
        var check = $parse(attrs.validationCheck);

        // Watch for changes to this input
        scope.$watch(check, function (newValue) {
          ngModel.$setValidity(attrs.name, newValue);
        });
      }
  }
}
]).run(function ($rootScope) {
    $rootScope.title = [];
    $rootScope.start = [];
})
.controller('RegisterCtrl', function($scope,$firebaseAuth, AUTHREF,$state, $ionicLoading,$firebaseObject,$location,$ionicSideMenuDelegate)  {
$ionicSideMenuDelegate.canDragContent(false);

$scope.register=function()
{
$ionicLoading.show({
      template: 'Registering'
    });

    var fname = $scope.fname;
    var lname = $scope.lname;
    /*var uname = $scope.uname;*/
    var emailid = $scope.emailid;
    var password = $scope.password;
      /*
       * Below Method is used for user creations and creating basic
       * profile of user in fire base
       */
    
    var fireRef = new Firebase("https://diametrix.firebaseio.com/");
    $scope.authObj = $firebaseAuth(fireRef);
    $scope.authObj.$createUser({
        email: emailid,
        password: password
      }).then(function(userData) {
        console.log("User " + userData.uid + " created successfully!");
       
        /* Creating Basic Profile under userid */
       
        var fireRefobj = new Firebase("https://diametrix.firebaseio.com/"+userData.uid+"/profile/");
        
        var fbobj = $firebaseObject(fireRefobj); 
        fbobj.fname = fname;
        fbobj.lname = lname;
        fbobj.dob = "";
        fbobj.maritalStatus = "";
        fbobj.height = "";
        fbobj.weight = "";
        fbobj.sex = "";
        fbobj.bloodgrp = "";
        fbobj.rptShare = "";
        fbobj.country = "";
        fbobj.mobileno = "";
        
        fbobj.$save().then(function(ref) {
          console.log(ref);

        }, function(error) {
          console.log("Error:", error);
        });
        
         var fbbobj = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/"+userData.uid+"/advsetting/")); 
         fbbobj.BloodPressure=true;
         fbbobj.Insulin=true;
         fbbobj.SugarCount=true;
         fbbobj.SleepingHrs=true;
         fbbobj.ExcersixeHrs=true;
         fbbobj.EyeCheckup=true;
         fbbobj.FootCheckup=true;
         fbbobj.$save().then(function(ref) {
            console.log(ref);

          }, function(error) {
            console.log("Error:", error);
          });
        
        return $scope.authObj.$authWithPassword({
          email: $scope.emailid,
          password: $scope.password
        });
      }).then(function(authData) {
        console.log("Logged in as:", authData.uid);
        $location.path('/profile');
      }).catch(function(error) {
        console.error("Error: ", error);
      });
    
  
$ionicLoading.hide();

}

})
.controller('forgotPsdCtrl', function($scope,$firebaseAuth, AUTHREF,$state, $ionicLoading,$firebaseObject,$location,$ionicSideMenuDelegate)  {
$ionicSideMenuDelegate.canDragContent(false); 
  $scope.sendForgotPsd = function(){

    $ionicLoading.show({
      template: 'sending request'
    });
    
    var fireRef = new Firebase("https://diametrix.firebaseio.com/");
    fireRef.resetPassword({
      email: $scope.forgotemailid
    }, function(error) {
      if (error) {
        switch (error.code) {
          case "INVALID_USER":
            console.log("The specified user account does not exist.");
            break;
          default:
            console.log("Error resetting password:", error);
        }
      } else {
        console.log("Password reset email sent successfully!");
      }
    });

    $ionicLoading.hide();
  }

})

.controller('NavCtrl', function($scope, $ionicSideMenuDelegate) {
  $scope.showMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
  /*$scope.showRightMenu = function () {
    $ionicSideMenuDelegate.toggleRight();
  };*/
})

.run(function($ionicPlatform, $rootScope, $cordovaVibration) {
  $ionicPlatform.ready(function() {

    // AuthService
    // AuthService.userIsLoggedIn().then(function(response)
    // {
    //   if(response === true)
    //   {
    //     $state.go('user');
    //   }
    //   else
    //   {
    //     $state.go('login');
    //   }
    // });

    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
     
     
    $rootScope.$on('$cordovaLocalNotification:trigger', function(event, notification, state){
      $cordovaVibration.vibrate(2000);
    });
    
  });
})

//Page Back disable 
.run(function($ionicPlatform, $rootScope, $cordovaVibration,$ionicPopup) {
    
    $ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name=="page1"){
  
  
  var confirmPopup = $ionicPopup.confirm({
       title: 'Consume Ice Cream',
       template: 'Are you sure you want to eat this ice cream?'
     });
     confirmPopup.then(function(res) {
       if(res) {
      navigator.app.exitApp();
       } else {
         console.log('You are not sure');
       }
     });   }
else 
{ 
     navigator.app.backHistory();
    }
  }, 100);
    
  });
