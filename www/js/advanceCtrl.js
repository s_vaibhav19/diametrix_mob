diametrix.controller('advanceCtrl', function($scope,$firebaseAuth, AUTHREF,$state,$firebaseObject)  {
	
    $scope.authObj = $firebaseAuth(AUTHREF);
	var authData = $scope.authObj.$getAuth();
	$scope.advsetting = $firebaseObject(new Firebase(AUTHREF+authData.uid+"/advsetting/"));

});