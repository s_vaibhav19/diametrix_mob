diametrix.controller('profileCtrl', function($scope,$firebaseAuth, AUTHREF,$state,$firebaseObject)  {
	
	/*var fireRef = new Firebase("https://diametrix.firebaseio.com/");*/
	$scope.authObj = $firebaseAuth(AUTHREF);
	var authData = $scope.authObj.$getAuth();
	var fireRefobj = $firebaseObject(new Firebase(AUTHREF+authData.uid+"/profile/"));
	var sex = "";
	var maritalStatus = "";
	var bloodgrp = "";
	var dob ="";
	fireRefobj.$loaded()
		  .then(function(data) {
			  $scope.height=data.height;
			  $scope.weight=data.weight;
			  $scope.rptShare = data.rptShare;
			  $scope.fname=data.fname;
			  $scope.lname=data.lname;
			  bloodgrp=data.bloodgrp;
			  sex=data.sex;
			  maritalStatus=data.maritalStatus;
			  dob = data.dob;
			  setDropDown(sex,maritalStatus,bloodgrp,dob);
		  })
		  .catch(function(error) {
		    console.error("Error:", error);
		  });
	  $scope.updateProfileData=function(event){
	  	 var fbobj = $firebaseObject(new Firebase(AUTHREF+authData.uid+"/profile/"));
		  fbobj.fname = $scope.fname;
		  fbobj.lname = $scope.lname;
		  fbobj.dob = document.getElementById('dob').value;
		  fbobj.maritalStatus = document.getElementById('maritalStatus').value;
		  fbobj.height = $scope.height;
		  fbobj.weight = $scope.weight;
		  fbobj.sex = document.getElementById('sex').value;
		  fbobj.bloodgrp = document.getElementById('bloodgrp').value;
		  fbobj.rptShare = $scope.rptShare;
		  		  
			fbobj.$save().then(function(ref) {
				console.log(ref);

			}, function(error) {
				console.log("Error:", error);
			});
			console.log($scope.fname+"<>"+$scope.lname+"<>"+$scope.height+"<>"+$scope.weight+"<>"+$scope.rptShare);
			console.log(document.getElementById('bloodgrp').value+"<>"+document.getElementById('sex').value+"<>"+document.getElementById('maritalStatus').value+"<>"+document.getElementById('dob').value);
	  }
});

function setDropDown(sex,maritalStatus,bloodgrp,dob){
	document.getElementById('sex').value=sex;
	document.getElementById('maritalStatus').value=maritalStatus;
	document.getElementById('bloodgrp').value=bloodgrp;
	document.getElementById('dob').value=dob;
}